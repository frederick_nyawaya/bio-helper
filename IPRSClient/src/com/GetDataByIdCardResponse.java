
package com;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetDataByIdCardResult" type="{http://schemas.datacontract.org/2004/07/IPRSManager}HumanInfoFromIDCard" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDataByIdCardResult"
})
@XmlRootElement(name = "GetDataByIdCardResponse")
public class GetDataByIdCardResponse {

    @XmlElementRef(name = "GetDataByIdCardResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<HumanInfoFromIDCard> getDataByIdCardResult;

    /**
     * Gets the value of the getDataByIdCardResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link HumanInfoFromIDCard }{@code >}
     *     
     */
    public JAXBElement<HumanInfoFromIDCard> getGetDataByIdCardResult() {
        return getDataByIdCardResult;
    }

    /**
     * Sets the value of the getDataByIdCardResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link HumanInfoFromIDCard }{@code >}
     *     
     */
    public void setGetDataByIdCardResult(JAXBElement<HumanInfoFromIDCard> value) {
        this.getDataByIdCardResult = value;
    }

}
