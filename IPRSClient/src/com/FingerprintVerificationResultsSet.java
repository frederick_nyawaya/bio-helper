
package com;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FingerprintVerificationResultsSet.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FingerprintVerificationResultsSet">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotMatch"/>
 *     &lt;enumeration value="Match"/>
 *     &lt;enumeration value="NoFingersInDb"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FingerprintVerificationResultsSet", namespace = "http://schemas.datacontract.org/2004/07/IPRSManager")
@XmlEnum
public enum FingerprintVerificationResultsSet {

    @XmlEnumValue("NotMatch")
    NOT_MATCH("NotMatch"),
    @XmlEnumValue("Match")
    MATCH("Match"),
    @XmlEnumValue("NoFingersInDb")
    NO_FINGERS_IN_DB("NoFingersInDb");
    private final String value;

    FingerprintVerificationResultsSet(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FingerprintVerificationResultsSet fromValue(String v) {
        for (FingerprintVerificationResultsSet c: FingerprintVerificationResultsSet.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
