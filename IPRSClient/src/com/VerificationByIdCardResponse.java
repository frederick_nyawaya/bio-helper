
package com;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VerificationByIdCardResult" type="{http://schemas.datacontract.org/2004/07/IPRSManager}FingerprintVerificationResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "verificationByIdCardResult"
})
@XmlRootElement(name = "VerificationByIdCardResponse")
public class VerificationByIdCardResponse {

    @XmlElementRef(name = "VerificationByIdCardResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<FingerprintVerificationResult> verificationByIdCardResult;

    /**
     * Gets the value of the verificationByIdCardResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FingerprintVerificationResult }{@code >}
     *     
     */
    public JAXBElement<FingerprintVerificationResult> getVerificationByIdCardResult() {
        return verificationByIdCardResult;
    }

    /**
     * Sets the value of the verificationByIdCardResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FingerprintVerificationResult }{@code >}
     *     
     */
    public void setVerificationByIdCardResult(JAXBElement<FingerprintVerificationResult> value) {
        this.verificationByIdCardResult = value;
    }

}
