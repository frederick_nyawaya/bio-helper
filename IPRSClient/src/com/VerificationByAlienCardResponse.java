
package com;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VerificationByAlienCardResult" type="{http://schemas.datacontract.org/2004/07/IPRSManager}FingerprintVerificationResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "verificationByAlienCardResult"
})
@XmlRootElement(name = "VerificationByAlienCardResponse")
public class VerificationByAlienCardResponse {

    @XmlElementRef(name = "VerificationByAlienCardResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<FingerprintVerificationResult> verificationByAlienCardResult;

    /**
     * Gets the value of the verificationByAlienCardResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FingerprintVerificationResult }{@code >}
     *     
     */
    public JAXBElement<FingerprintVerificationResult> getVerificationByAlienCardResult() {
        return verificationByAlienCardResult;
    }

    /**
     * Sets the value of the verificationByAlienCardResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FingerprintVerificationResult }{@code >}
     *     
     */
    public void setVerificationByAlienCardResult(JAXBElement<FingerprintVerificationResult> value) {
        this.verificationByAlienCardResult = value;
    }

}
