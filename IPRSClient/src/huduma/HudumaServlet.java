package huduma;

import ecs.*;
import huduma.wsobjects.HudumaWsFault_Exception;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by frederick on 2/25/16.
 */
public class HudumaServlet extends HttpServlet {

    HudumaClient client;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            processRequest(req, resp);
        } catch (HudumaWsFault_Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            processRequest(req, resp);
        } catch (HudumaWsFault_Exception e) {
            e.printStackTrace();
        }

    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException, HudumaWsFault_Exception {

        resp.setContentType("application/json;charset=UTF-8");

        int requestCode = Integer.parseInt(req.getParameter(Constants.REQUEST));

        String data = req.getParameter(Constants.DATA);
        String result = "";

        try (PrintWriter out = resp.getWriter()) {
            result = client.handleRequest(requestCode, data);
            out.print(result);

        }
    }
}
