package huduma;

import com.google.gson.Gson;
import huduma.wsobjects.*;

/**
 * Created by frederick on 2/25/16.
 */
public class HudumaClient {

    private static final String USER_NAME = "";
    private static final String PASSWORD = "";
    private static final String NAMESPACE_URI = "";

    private static HudumaWebService service;


    //login is required once for every session
//    public static Boolean init() {
//        //replace NAMESPACE_URI with the actual server url and port that supports binding
//        service = new ServerInterface().getPort(new QName(NAMESPACE_URI, "WSHttpBinding_IServiceIPRS"), HudumaWebService.class);
//        return service.login(USER_NAME, PASSWORD);//preferably passed alongside our http params as they might change
//    }

    public static String handleRequest(int type, String request) throws HudumaWsFault_Exception {
        String res = "";

        //TODO pass auth credentials before any of this
        switch (type) {
            case Constants.CREATE_HUDUMA_CUSTOMER:
                res = new Gson().toJson(createHudumaCustomer(new Gson().fromJson(request, CreateCustomerReq.class)));
                break;
            case Constants.UPDATE_HUDUMA_CUSTOMER:
                res = new Gson().toJson(updateHudumaCustomer(new Gson().fromJson(request, UpdateCustomerReq.class)));
                break;
            case Constants.DELETE_HUDUMA_CUSTOMER:
                res = new Gson().toJson(deleteCustomer(new Gson().fromJson(request, DeleteCustomerReq.class)));
                break;
            case Constants.GET_HUDUMA_CUSTOMER:
                res = new Gson().toJson(getCustomer(new Gson().fromJson(request, GetCustomerReq.class)));
                break;
            case Constants.UPDATE_CUSTOMER_CARD:
                res = new Gson().toJson(updateCustomerCard(new Gson().fromJson(request, UpdateCustomerCardReq.class)));
                break;
            case Constants.IS_EXIST_HUDUMA_CUSTOMER:
                res = new Gson().toJson(isCustomerExist(new Gson().fromJson(request, IsCustomerExistReq.class)));
                break;

        }

        return res;

    }


    public static CreateCustomerRes createHudumaCustomer(CreateCustomerReq request) throws HudumaWsFault_Exception {
        return service.createHudumaCustomer(request);
    }

    public static UpdateCustomerRes updateHudumaCustomer(UpdateCustomerReq request) throws HudumaWsFault_Exception {
        return service.updateHudumaCustomer(request);
    }

    public static DeleteCustomerRes deleteCustomer(DeleteCustomerReq request) throws HudumaWsFault_Exception {
        return service.deleteCustomer(request);
    }

    public static GetCustomerRes getCustomer(GetCustomerReq request) throws HudumaWsFault_Exception {
        return service.getCustomer(request);
    }

    public static IsCustomerExistRes isCustomerExist(IsCustomerExistReq request) throws HudumaWsFault_Exception {
        return service.isCustomerExist(request);
    }

    public static UpdateCustomerCardRes updateCustomerCard(UpdateCustomerCardReq request) throws HudumaWsFault_Exception {
        return service.updateCustomerCard(request);
    }
}
