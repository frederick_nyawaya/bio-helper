
package huduma.wsobjects;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for customerFingerPrints complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="customerFingerPrints">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FingerPrints" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FingerPrint" type="{http://ws.pgw.crestwavetech.ru/}fingerPrint" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="MissingReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "customerFingerPrints", propOrder = {
    "fingerPrints",
    "missingReason"
})
public class CustomerFingerPrints {

    @XmlElement(name = "FingerPrints")
    protected CustomerFingerPrints.FingerPrints fingerPrints;
    @XmlElement(name = "MissingReason")
    protected String missingReason;

    /**
     * Gets the value of the fingerPrints property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerFingerPrints.FingerPrints }
     *     
     */
    public CustomerFingerPrints.FingerPrints getFingerPrints() {
        return fingerPrints;
    }

    /**
     * Sets the value of the fingerPrints property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerFingerPrints.FingerPrints }
     *     
     */
    public void setFingerPrints(CustomerFingerPrints.FingerPrints value) {
        this.fingerPrints = value;
    }

    /**
     * Gets the value of the missingReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMissingReason() {
        return missingReason;
    }

    /**
     * Sets the value of the missingReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMissingReason(String value) {
        this.missingReason = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FingerPrint" type="{http://ws.pgw.crestwavetech.ru/}fingerPrint" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fingerPrint"
    })
    public static class FingerPrints {

        @XmlElement(name = "FingerPrint")
        protected List<FingerPrint> fingerPrint;

        /**
         * Gets the value of the fingerPrint property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fingerPrint property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFingerPrint().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FingerPrint }
         * 
         * 
         */
        public List<FingerPrint> getFingerPrint() {
            if (fingerPrint == null) {
                fingerPrint = new ArrayList<FingerPrint>();
            }
            return this.fingerPrint;
        }

    }

}
