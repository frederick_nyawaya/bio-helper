
package huduma.wsobjects;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fingerType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="fingerType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="RIGHT_THUMB"/>
 *     &lt;enumeration value="RIGHT_INDEX"/>
 *     &lt;enumeration value="RIGHT_MIDDLE"/>
 *     &lt;enumeration value="RIGHT_RING"/>
 *     &lt;enumeration value="RIGHT_LITTLE"/>
 *     &lt;enumeration value="LEFT_THUMB"/>
 *     &lt;enumeration value="LEFT_INDEX"/>
 *     &lt;enumeration value="LEFT_MIDDLE"/>
 *     &lt;enumeration value="LEFT_RING"/>
 *     &lt;enumeration value="LEFT_LITTLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "fingerType")
@XmlEnum
public enum FingerType {

    RIGHT_THUMB,
    RIGHT_INDEX,
    RIGHT_MIDDLE,
    RIGHT_RING,
    RIGHT_LITTLE,
    LEFT_THUMB,
    LEFT_INDEX,
    LEFT_MIDDLE,
    LEFT_RING,
    LEFT_LITTLE;

    public String value() {
        return name();
    }

    public static FingerType fromValue(String v) {
        return valueOf(v);
    }

}
