
package huduma.wsobjects;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the huduma package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DeleteCustomerRes_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "DeleteCustomerRes");
    private final static QName _CreateHudumaCustomer_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "createHudumaCustomer");
    private final static QName _DeleteCustomer_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "deleteCustomer");
    private final static QName _DeleteCustomerReq_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "DeleteCustomerReq");
    private final static QName _GetCustomer_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "getCustomer");
    private final static QName _UpdateCustomerCardResponse_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "updateCustomerCardResponse");
    private final static QName _IsCustomerExist_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "isCustomerExist");
    private final static QName _CreateCustomerReq_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "CreateCustomerReq");
    private final static QName _CreateCustomerRes_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "CreateCustomerRes");
    private final static QName _UpdateHudumaCustomer_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "updateHudumaCustomer");
    private final static QName _GetCustomerRes_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "GetCustomerRes");
    private final static QName _GetCustomerReq_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "GetCustomerReq");
    private final static QName _IsCustomerExistResponse_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "isCustomerExistResponse");
    private final static QName _UpdateCustomerCardRes_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "UpdateCustomerCardRes");
    private final static QName _UpdateCustomerCard_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "updateCustomerCard");
    private final static QName _UpdateCustomerRes_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "UpdateCustomerRes");
    private final static QName _UpdateCustomerReq_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "UpdateCustomerReq");
    private final static QName _GetCustomerResponse_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "getCustomerResponse");
    private final static QName _UpdateCustomerCardReq_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "UpdateCustomerCardReq");
    private final static QName _IsCustomerExistReq_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "IsCustomerExistReq");
    private final static QName _CreateHudumaCustomerResponse_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "createHudumaCustomerResponse");
    private final static QName _IsCustomerExistRes_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "IsCustomerExistRes");
    private final static QName _DeleteCustomerResponse_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "deleteCustomerResponse");
    private final static QName _HudumaWsFault_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "HudumaWsFault");
    private final static QName _UpdateHudumaCustomerResponse_QNAME = new QName("http://ws.pgw.crestwavetech.ru/", "updateHudumaCustomerResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: huduma
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CustomerFingerPrints }
     * 
     */
    public CustomerFingerPrints createCustomerFingerPrints() {
        return new CustomerFingerPrints();
    }

    /**
     * Create an instance of {@link UpdateCustomerCardReq }
     * 
     */
    public UpdateCustomerCardReq createUpdateCustomerCardReq() {
        return new UpdateCustomerCardReq();
    }

    /**
     * Create an instance of {@link GetCustomerResponse }
     * 
     */
    public GetCustomerResponse createGetCustomerResponse() {
        return new GetCustomerResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomerReq }
     * 
     */
    public UpdateCustomerReq createUpdateCustomerReq() {
        return new UpdateCustomerReq();
    }

    /**
     * Create an instance of {@link IsCustomerExistReq }
     * 
     */
    public IsCustomerExistReq createIsCustomerExistReq() {
        return new IsCustomerExistReq();
    }

    /**
     * Create an instance of {@link IsCustomerExistResponse }
     * 
     */
    public IsCustomerExistResponse createIsCustomerExistResponse() {
        return new IsCustomerExistResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomerRes }
     * 
     */
    public UpdateCustomerRes createUpdateCustomerRes() {
        return new UpdateCustomerRes();
    }

    /**
     * Create an instance of {@link UpdateCustomerCardRes }
     * 
     */
    public UpdateCustomerCardRes createUpdateCustomerCardRes() {
        return new UpdateCustomerCardRes();
    }

    /**
     * Create an instance of {@link UpdateCustomerCard }
     * 
     */
    public UpdateCustomerCard createUpdateCustomerCard() {
        return new UpdateCustomerCard();
    }

    /**
     * Create an instance of {@link UpdateHudumaCustomerResponse }
     * 
     */
    public UpdateHudumaCustomerResponse createUpdateHudumaCustomerResponse() {
        return new UpdateHudumaCustomerResponse();
    }

    /**
     * Create an instance of {@link IsCustomerExistRes }
     * 
     */
    public IsCustomerExistRes createIsCustomerExistRes() {
        return new IsCustomerExistRes();
    }

    /**
     * Create an instance of {@link CreateHudumaCustomerResponse }
     * 
     */
    public CreateHudumaCustomerResponse createCreateHudumaCustomerResponse() {
        return new CreateHudumaCustomerResponse();
    }

    /**
     * Create an instance of {@link HudumaWsFault }
     * 
     */
    public HudumaWsFault createHudumaWsFault() {
        return new HudumaWsFault();
    }

    /**
     * Create an instance of {@link DeleteCustomerResponse }
     * 
     */
    public DeleteCustomerResponse createDeleteCustomerResponse() {
        return new DeleteCustomerResponse();
    }

    /**
     * Create an instance of {@link IsCustomerExist }
     * 
     */
    public IsCustomerExist createIsCustomerExist() {
        return new IsCustomerExist();
    }

    /**
     * Create an instance of {@link CreateCustomerRes }
     * 
     */
    public CreateCustomerRes createCreateCustomerRes() {
        return new CreateCustomerRes();
    }

    /**
     * Create an instance of {@link CreateCustomerReq }
     * 
     */
    public CreateCustomerReq createCreateCustomerReq() {
        return new CreateCustomerReq();
    }

    /**
     * Create an instance of {@link DeleteCustomerReq }
     * 
     */
    public DeleteCustomerReq createDeleteCustomerReq() {
        return new DeleteCustomerReq();
    }

    /**
     * Create an instance of {@link CreateHudumaCustomer }
     * 
     */
    public CreateHudumaCustomer createCreateHudumaCustomer() {
        return new CreateHudumaCustomer();
    }

    /**
     * Create an instance of {@link DeleteCustomer }
     * 
     */
    public DeleteCustomer createDeleteCustomer() {
        return new DeleteCustomer();
    }

    /**
     * Create an instance of {@link DeleteCustomerRes }
     * 
     */
    public DeleteCustomerRes createDeleteCustomerRes() {
        return new DeleteCustomerRes();
    }

    /**
     * Create an instance of {@link UpdateCustomerCardResponse }
     * 
     */
    public UpdateCustomerCardResponse createUpdateCustomerCardResponse() {
        return new UpdateCustomerCardResponse();
    }

    /**
     * Create an instance of {@link GetCustomer }
     * 
     */
    public GetCustomer createGetCustomer() {
        return new GetCustomer();
    }

    /**
     * Create an instance of {@link GetCustomerRes }
     * 
     */
    public GetCustomerRes createGetCustomerRes() {
        return new GetCustomerRes();
    }

    /**
     * Create an instance of {@link GetCustomerReq }
     * 
     */
    public GetCustomerReq createGetCustomerReq() {
        return new GetCustomerReq();
    }

    /**
     * Create an instance of {@link UpdateHudumaCustomer }
     * 
     */
    public UpdateHudumaCustomer createUpdateHudumaCustomer() {
        return new UpdateHudumaCustomer();
    }

    /**
     * Create an instance of {@link CardDetails }
     * 
     */
    public CardDetails createCardDetails() {
        return new CardDetails();
    }

    /**
     * Create an instance of {@link ExtendedDetails }
     * 
     */
    public ExtendedDetails createExtendedDetails() {
        return new ExtendedDetails();
    }

    /**
     * Create an instance of {@link Response }
     * 
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link FingerPrint }
     * 
     */
    public FingerPrint createFingerPrint() {
        return new FingerPrint();
    }

    /**
     * Create an instance of {@link CustomerFingerPrints.FingerPrints }
     * 
     */
    public CustomerFingerPrints.FingerPrints createCustomerFingerPrintsFingerPrints() {
        return new CustomerFingerPrints.FingerPrints();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCustomerRes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "DeleteCustomerRes")
    public JAXBElement<DeleteCustomerRes> createDeleteCustomerRes(DeleteCustomerRes value) {
        return new JAXBElement<DeleteCustomerRes>(_DeleteCustomerRes_QNAME, DeleteCustomerRes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateHudumaCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "createHudumaCustomer")
    public JAXBElement<CreateHudumaCustomer> createCreateHudumaCustomer(CreateHudumaCustomer value) {
        return new JAXBElement<CreateHudumaCustomer>(_CreateHudumaCustomer_QNAME, CreateHudumaCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "deleteCustomer")
    public JAXBElement<DeleteCustomer> createDeleteCustomer(DeleteCustomer value) {
        return new JAXBElement<DeleteCustomer>(_DeleteCustomer_QNAME, DeleteCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCustomerReq }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "DeleteCustomerReq")
    public JAXBElement<DeleteCustomerReq> createDeleteCustomerReq(DeleteCustomerReq value) {
        return new JAXBElement<DeleteCustomerReq>(_DeleteCustomerReq_QNAME, DeleteCustomerReq.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "getCustomer")
    public JAXBElement<GetCustomer> createGetCustomer(GetCustomer value) {
        return new JAXBElement<GetCustomer>(_GetCustomer_QNAME, GetCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerCardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "updateCustomerCardResponse")
    public JAXBElement<UpdateCustomerCardResponse> createUpdateCustomerCardResponse(UpdateCustomerCardResponse value) {
        return new JAXBElement<UpdateCustomerCardResponse>(_UpdateCustomerCardResponse_QNAME, UpdateCustomerCardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsCustomerExist }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "isCustomerExist")
    public JAXBElement<IsCustomerExist> createIsCustomerExist(IsCustomerExist value) {
        return new JAXBElement<IsCustomerExist>(_IsCustomerExist_QNAME, IsCustomerExist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomerReq }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "CreateCustomerReq")
    public JAXBElement<CreateCustomerReq> createCreateCustomerReq(CreateCustomerReq value) {
        return new JAXBElement<CreateCustomerReq>(_CreateCustomerReq_QNAME, CreateCustomerReq.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomerRes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "CreateCustomerRes")
    public JAXBElement<CreateCustomerRes> createCreateCustomerRes(CreateCustomerRes value) {
        return new JAXBElement<CreateCustomerRes>(_CreateCustomerRes_QNAME, CreateCustomerRes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateHudumaCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "updateHudumaCustomer")
    public JAXBElement<UpdateHudumaCustomer> createUpdateHudumaCustomer(UpdateHudumaCustomer value) {
        return new JAXBElement<UpdateHudumaCustomer>(_UpdateHudumaCustomer_QNAME, UpdateHudumaCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerRes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "GetCustomerRes")
    public JAXBElement<GetCustomerRes> createGetCustomerRes(GetCustomerRes value) {
        return new JAXBElement<GetCustomerRes>(_GetCustomerRes_QNAME, GetCustomerRes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerReq }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "GetCustomerReq")
    public JAXBElement<GetCustomerReq> createGetCustomerReq(GetCustomerReq value) {
        return new JAXBElement<GetCustomerReq>(_GetCustomerReq_QNAME, GetCustomerReq.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsCustomerExistResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "isCustomerExistResponse")
    public JAXBElement<IsCustomerExistResponse> createIsCustomerExistResponse(IsCustomerExistResponse value) {
        return new JAXBElement<IsCustomerExistResponse>(_IsCustomerExistResponse_QNAME, IsCustomerExistResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerCardRes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "UpdateCustomerCardRes")
    public JAXBElement<UpdateCustomerCardRes> createUpdateCustomerCardRes(UpdateCustomerCardRes value) {
        return new JAXBElement<UpdateCustomerCardRes>(_UpdateCustomerCardRes_QNAME, UpdateCustomerCardRes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "updateCustomerCard")
    public JAXBElement<UpdateCustomerCard> createUpdateCustomerCard(UpdateCustomerCard value) {
        return new JAXBElement<UpdateCustomerCard>(_UpdateCustomerCard_QNAME, UpdateCustomerCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerRes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "UpdateCustomerRes")
    public JAXBElement<UpdateCustomerRes> createUpdateCustomerRes(UpdateCustomerRes value) {
        return new JAXBElement<UpdateCustomerRes>(_UpdateCustomerRes_QNAME, UpdateCustomerRes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerReq }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "UpdateCustomerReq")
    public JAXBElement<UpdateCustomerReq> createUpdateCustomerReq(UpdateCustomerReq value) {
        return new JAXBElement<UpdateCustomerReq>(_UpdateCustomerReq_QNAME, UpdateCustomerReq.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "getCustomerResponse")
    public JAXBElement<GetCustomerResponse> createGetCustomerResponse(GetCustomerResponse value) {
        return new JAXBElement<GetCustomerResponse>(_GetCustomerResponse_QNAME, GetCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerCardReq }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "UpdateCustomerCardReq")
    public JAXBElement<UpdateCustomerCardReq> createUpdateCustomerCardReq(UpdateCustomerCardReq value) {
        return new JAXBElement<UpdateCustomerCardReq>(_UpdateCustomerCardReq_QNAME, UpdateCustomerCardReq.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsCustomerExistReq }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "IsCustomerExistReq")
    public JAXBElement<IsCustomerExistReq> createIsCustomerExistReq(IsCustomerExistReq value) {
        return new JAXBElement<IsCustomerExistReq>(_IsCustomerExistReq_QNAME, IsCustomerExistReq.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateHudumaCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "createHudumaCustomerResponse")
    public JAXBElement<CreateHudumaCustomerResponse> createCreateHudumaCustomerResponse(CreateHudumaCustomerResponse value) {
        return new JAXBElement<CreateHudumaCustomerResponse>(_CreateHudumaCustomerResponse_QNAME, CreateHudumaCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsCustomerExistRes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "IsCustomerExistRes")
    public JAXBElement<IsCustomerExistRes> createIsCustomerExistRes(IsCustomerExistRes value) {
        return new JAXBElement<IsCustomerExistRes>(_IsCustomerExistRes_QNAME, IsCustomerExistRes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "deleteCustomerResponse")
    public JAXBElement<DeleteCustomerResponse> createDeleteCustomerResponse(DeleteCustomerResponse value) {
        return new JAXBElement<DeleteCustomerResponse>(_DeleteCustomerResponse_QNAME, DeleteCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HudumaWsFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "HudumaWsFault")
    public JAXBElement<HudumaWsFault> createHudumaWsFault(HudumaWsFault value) {
        return new JAXBElement<HudumaWsFault>(_HudumaWsFault_QNAME, HudumaWsFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateHudumaCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pgw.crestwavetech.ru/", name = "updateHudumaCustomerResponse")
    public JAXBElement<UpdateHudumaCustomerResponse> createUpdateHudumaCustomerResponse(UpdateHudumaCustomerResponse value) {
        return new JAXBElement<UpdateHudumaCustomerResponse>(_UpdateHudumaCustomerResponse_QNAME, UpdateHudumaCustomerResponse.class, null, value);
    }

}
