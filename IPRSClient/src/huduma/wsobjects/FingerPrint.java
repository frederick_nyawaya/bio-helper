
package huduma.wsobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fingerPrint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fingerPrint">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FingerImage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FingerType" type="{http://ws.pgw.crestwavetech.ru/}fingerType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fingerPrint", propOrder = {
    "fingerImage",
    "fingerType"
})
public class FingerPrint {

    @XmlElement(name = "FingerImage")
    protected String fingerImage;
    @XmlElement(name = "FingerType")
    @XmlSchemaType(name = "string")
    protected FingerType fingerType;

    /**
     * Gets the value of the fingerImage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFingerImage() {
        return fingerImage;
    }

    /**
     * Sets the value of the fingerImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFingerImage(String value) {
        this.fingerImage = value;
    }

    /**
     * Gets the value of the fingerType property.
     * 
     * @return
     *     possible object is
     *     {@link FingerType }
     *     
     */
    public FingerType getFingerType() {
        return fingerType;
    }

    /**
     * Sets the value of the fingerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FingerType }
     *     
     */
    public void setFingerType(FingerType value) {
        this.fingerType = value;
    }

}
