
package huduma.wsobjects;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for responseStatusEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="responseStatusEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SUCCESS"/>
 *     &lt;enumeration value="SYSTEM_INTERNAL_ERROR"/>
 *     &lt;enumeration value="PERSON_IS_NOT_VALID"/>
 *     &lt;enumeration value="PERSON_ALREADY_REGISTERED"/>
 *     &lt;enumeration value="FIELD_VALIDATION_ERROR"/>
 *     &lt;enumeration value="INVALID_XML_STRUCTURE"/>
 *     &lt;enumeration value="PERSON_NOT_FOUND"/>
 *     &lt;enumeration value="AUTHENTICATION_ERROR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "responseStatusEnum")
@XmlEnum
public enum ResponseStatusEnum {

    SUCCESS,
    SYSTEM_INTERNAL_ERROR,
    PERSON_IS_NOT_VALID,
    PERSON_ALREADY_REGISTERED,
    FIELD_VALIDATION_ERROR,
    INVALID_XML_STRUCTURE,
    PERSON_NOT_FOUND,
    AUTHENTICATION_ERROR;

    public String value() {
        return name();
    }

    public static ResponseStatusEnum fromValue(String v) {
        return valueOf(v);
    }

}
