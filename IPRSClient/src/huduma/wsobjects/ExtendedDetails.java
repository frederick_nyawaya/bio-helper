
package huduma.wsobjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for extendedDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="extendedDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Photo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerFingerPrints" type="{http://ws.pgw.crestwavetech.ru/}customerFingerPrints" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "extendedDetails", propOrder = {
    "photo",
    "customerFingerPrints"
})
public class ExtendedDetails {

    @XmlElement(name = "Photo")
    protected String photo;
    @XmlElement(name = "CustomerFingerPrints")
    protected CustomerFingerPrints customerFingerPrints;

    /**
     * Gets the value of the photo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * Sets the value of the photo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoto(String value) {
        this.photo = value;
    }

    /**
     * Gets the value of the customerFingerPrints property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerFingerPrints }
     *     
     */
    public CustomerFingerPrints getCustomerFingerPrints() {
        return customerFingerPrints;
    }

    /**
     * Sets the value of the customerFingerPrints property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerFingerPrints }
     *     
     */
    public void setCustomerFingerPrints(CustomerFingerPrints value) {
        this.customerFingerPrints = value;
    }

}
