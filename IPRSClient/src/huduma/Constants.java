package huduma;

/**
 * Created by Frederick on 1/27/2016.
 */
public class Constants {

    public static final int CREATE_HUDUMA_CUSTOMER = 6;
    public static final int UPDATE_HUDUMA_CUSTOMER = 7;
    public static final int DELETE_HUDUMA_CUSTOMER = 8;

    public static final int GET_HUDUMA_CUSTOMER = 9;
    public static final int IS_EXIST_HUDUMA_CUSTOMER = 10;
    public static final int UPDATE_CUSTOMER_CARD = 11;

    public static final String REQUEST = "request";
    public static final String DATA = "data";



}
