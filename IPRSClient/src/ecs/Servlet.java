package ecs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Frederick on 1/27/2016.
 */
public class Servlet extends HttpServlet {

    IPRSClient client = new IPRSClient();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        response.setContentType("application/json;charset=UTF-8");

        int requestCode = Integer.parseInt(request.getParameter(Constants.REQUEST));

        String idNumber = request.getParameter(Constants.REQUEST);
        String serialNumber = request.getParameter(Constants.REQUEST);

        String commaSeparatedFingerprints = "";

        if (requestCode > Constants.ACTION_DATA_BY_ALIEN_CARD)//values greater than alient car imply verification
            commaSeparatedFingerprints = request.getParameter(Constants.REQUEST);

        String result = "";

        try (PrintWriter out = response.getWriter()) {

            if (client.init()) {
                result = client.handleRequest(requestCode, idNumber, serialNumber, commaSeparatedFingerprints);
            } else {
                result = "login failed";
            }


            out.print(result);

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


}
