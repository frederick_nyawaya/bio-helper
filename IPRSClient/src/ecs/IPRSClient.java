package ecs;

import com.ArrayOfbase64Binary;
import com.IServiceIPRS;
import com.ServerInterface;
import com.google.gson.Gson;
import com.sun.istack.internal.Nullable;
import org.apache.commons.codec.binary.Base64;

import javax.xml.namespace.QName;

/**
 * Created by Frederick on 1/22/2016.
 */
public class IPRSClient {
    public static final String NAMESPACE_URI = "http://tempuri.org/";
    public static final String USER_NAME = "";
    public static final String PASSWORD = "";

    static IServiceIPRS service;

    public static void main(String[] argv) {

        init();
    }


    /**
     * @param requestCode
     * @param idNumber
     * @param serialNumber
     * @param commaSeparatedFingerPrints
     * @return json encoded result
     */
    public String handleRequest(int requestCode, String idNumber, String serialNumber, @Nullable String commaSeparatedFingerPrints) {
        String result = "";

        switch (requestCode) {
            case Constants.ACTION_DATA_BY_ALIEN_CARD:
                result = getDataByAlienCard(idNumber, serialNumber);
                break;
            case Constants.ACTION_DATA_BY_ID:
                result = getDataByIdCard(idNumber, serialNumber);
                break;
            case Constants.ACTION_DATA_BY_PASSPORT:
                result = getDataByPassport(idNumber, serialNumber);
                break;
            case Constants.ACTION_VERIFICATION_BY_ALIEN_CARD:
                result = verificationByAlienCard(idNumber, serialNumber, getBase64Fingerprints(commaSeparatedFingerPrints));
                break;
            case Constants.ACTION_VERIFICATION_BY_ID:
                result = verificationByIdCard(idNumber, serialNumber, getBase64Fingerprints(commaSeparatedFingerPrints));
                break;
            case Constants.ACTION_VERIFICATION_BY_PASSPORT:
                result = verificationByPassport(idNumber, serialNumber, getBase64Fingerprints(commaSeparatedFingerPrints));
                break;
        }

        return result;
    }

    //login is required once for every session
    public static Boolean init() {
        //replace NAMESPACE_URI with the actual server url and port that supports binding
        service = new ServerInterface().getPort(new QName(NAMESPACE_URI, "WSHttpBinding_IServiceIPRS"), IServiceIPRS.class);
        return service.login(USER_NAME, PASSWORD);//preferably passed alongside our http params as they might change
    }

    /**
     * Takes comma separated finger prints, gets the bytes from each one of them, encodes them in base64.
     *
     * @param fingerprints
     * @return the list of encoded byte[]'s
     */
    private ArrayOfbase64Binary getBase64Fingerprints(String fingerprints) {
        String[] prints = fingerprints.split(",");
        ArrayOfbase64Binary result = new ArrayOfbase64Binary();

        for (int i = 0; i < prints.length; i++) {
            byte[] print = prints[i].getBytes();
            //the getBase64Binary method returns a reference to the object
            result.getBase64Binary().add(Base64.encodeBase64(print));

        }
        return result;
    }


    /**
     * @param idNumber
     * @param serialNumber
     * @return JSON encoded string
     * Returns data about person in class HumanInfoFromIDCard in case of successful search result in IPRS database. Field ErrorOccurred is False, ErrorCode, ErrorMessage is empty.
     * <p/>
     * Returns empty fields in case of method invocation failure.
     * Field ErrorOccurred is True, ErrorCode contains code of error, ErrorMessage contains message of error occurred.
     */
    private String getDataByIdCard(String idNumber, String serialNumber) {
        return new Gson().toJson(service.getDataByIdCard(idNumber, serialNumber));
    }

    /**
     * @param idNumber
     * @param serialNumber
     * @return JSON encoded string:
     */
    private String getDataByAlienCard(String idNumber, String serialNumber) {
        return new Gson().toJson(service.getDataByAlienCard(idNumber, serialNumber));
    }

    private String getDataByPassport(String idNumber, String serialNumber) {
        return new Gson().toJson(service.getDataByPassport(idNumber, serialNumber));
    }

    private String verificationByIdCard(String idNumber, String serialNumber, ArrayOfbase64Binary fingerPrints) {
        return new Gson().toJson(service.verificationByIdCard(idNumber, serialNumber, fingerPrints));
    }

    private String verificationByAlienCard(String idNumber, String serialNumber, ArrayOfbase64Binary fingerPrints) {
        return new Gson().toJson(service.verificationByAlienCard(idNumber, serialNumber, fingerPrints));
    }

    private String verificationByPassport(String idNumber, String serialNumber, ArrayOfbase64Binary fingerPrints) {
        return new Gson().toJson(service.verificationByPassport(idNumber, serialNumber, fingerPrints));
    }
}
