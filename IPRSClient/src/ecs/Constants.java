package ecs;

/**
 * Created by Frederick on 1/27/2016.
 */
public class Constants {

    public static final int ACTION_DATA_BY_ID = 0;
    public static final int ACTION_DATA_BY_PASSPORT = 1;
    public static final int ACTION_DATA_BY_ALIEN_CARD = 2;

    public static final int ACTION_VERIFICATION_BY_ID = 3;
    public static final int ACTION_VERIFICATION_BY_PASSPORT = 4;
    public static final int ACTION_VERIFICATION_BY_ALIEN_CARD = 5;

    public static final String REQUEST = "request";
    public static final String ID_NUMBER = "id_number";
    public static final String SERIAL_NUMBER = "serial_number";
    public static final String FINGERPRINTS_CS = "fingerprints";




}
