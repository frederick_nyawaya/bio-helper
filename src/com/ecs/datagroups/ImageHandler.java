package com.ecs.datagroups;

import com.github.jaiimageio.jpeg2000.J2KImageWriteParam;
import com.github.sarxos.webcam.Webcam;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;

/**
 * Reads the image from the web cam or file. Compresses the file to below the maximum
 * allowed size and saves two files in JPEG 2000 format, one uncompressed.
 * <p/>
 * Created by Frederick on 12/22/2015.
 */
public class ImageHandler {

    public static final int SOURCE_WEBCAM = 1;
    public static final int SOURCE_FILE = 2;


    private int source, maxSizeKB = 9;
    private File output;
    private File input;
    private File compressedImageFile;
    private BufferedImage image;


    /**
     * Preffered constructor, uses the default settings. Be sure to include the .jp2 extension to the file names
     * @param output
     * @param outputCompressed
     */
    public ImageHandler(File output, File outputCompressed) {
        this.output = output;
        this.compressedImageFile = outputCompressed;
        this.source = SOURCE_WEBCAM;

    }

    public ImageHandler(int source, File output, File input, File compressedImageFile) {
        this.source = source;
        this.output = output;
        this.input = input;
        this.compressedImageFile = compressedImageFile;
    }

    public File getInput() {

        return input;
    }

    public void setInput(File input) {
        this.input = input;
    }

    /**
     * Handles the opertaion
     *
     * @return the captured or read image
     * @throws IOException
     */
    public BufferedImage execute() throws IOException {
        if (source == SOURCE_FILE && input == null) {
            throw new NullPointerException("You need to specify the input file");
        }

        if (source == SOURCE_WEBCAM) {
            Webcam webcam = Webcam.getDefault();
            webcam.setViewSize(new Dimension(320, 240));
            webcam.open();
            image = webcam.getImage();
            webcam.close();

        } else if (source == SOURCE_FILE) {
            image = ImageIO.read(input);
        }

        writeImage();
        process();
        return image;

    }

    private BufferedImage writeImage() throws IOException {
        ImageIO.write(image, "JPEG 2000", output);
        return image;
    }

    /**
     * Maintains a loop to progressively compress the image until the size is below the maximum allowed
     *
     * @throws IOException
     */
    private void process() throws IOException {
        float quality = 1.0f;
        long size = output.length();
        System.out.println("raw: " + size);


        if (size > maxSizeKB * 1024) {
            //required initial compression using the highest quality factor
            compressImage(quality);

            while (compressedImageFile.length() > maxSizeKB * 1024) {
                //reduce quality factor progressively
                quality -= 0.1f;
                compressImage(quality);
                System.out.println("quality : " + quality);
            }
        }


        System.out.println("compressed: " + compressedImageFile.length());

    }

    /**
     * Does the compression, saves as jpeg 2000
     *
     * @param quality
     * @throws IOException
     */
    private void compressImage(float quality) throws IOException {
        OutputStream os = new FileOutputStream(compressedImageFile);

        Iterator writers = ImageIO.getImageWritersByFormatName("JPEG 2000");
        ImageWriter writer = (ImageWriter) writers.next();

        ImageOutputStream ios = ImageIO.createImageOutputStream(os);
        writer.setOutput(ios);

        J2KImageWriteParam writeParams = (J2KImageWriteParam) writer.getDefaultWriteParam();
        writeParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        writeParams.setLossless(false);//favour reduction in file size
        writeParams.setCompressionType("JPEG2000");
        writeParams.setCompressionQuality(quality);
        writeParams.setEncodingRate(quality);

        writer.write(null, new IIOImage(image, null, null), writeParams);

        ios.close();
        os.close();
        writer.dispose();

    }

    private BufferedImage resizeImage(BufferedImage image, int width, int height) {
        int type = 0;
        type = image.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : image.getType();
        BufferedImage resizedImage = new BufferedImage(width, height, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(image, 0, 0, width, height, null);
        g.dispose();
        return resizedImage;
    }
}
