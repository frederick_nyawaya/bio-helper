package com.ecs.datagroups;

import com.github.jaiimageio.jpeg2000.J2KImageWriteParam;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by Frederick on 1/6/2016.
 */
public class DG2Provider {

    //modify these indices with the length values, incrementally from the last one:
    //2     - for the whole template
    //5     - bio info group all i
    //11    - first bio info all in index 25 plus indices upto 11
    //25    - bio data length, data in index 26
    private static final String[] DATA_PAYLOAD = {
            "75",   // this is the Data Group 2 tag
            "82",
            "1AA5", //length --> this index shal be modified
            "7F61", //Biometric Information Group Template. Nested template (tag)
            "82",
            "1AA0", //length --> this index shal be modified
            "02",   //Defines the total number of objects stored as Biometric Information Templates that follow.
            "01",   //length
            "01",   //one facial image stored (value)
            "7F60", //First biometric information template where cc is the total length of the entire BIT (tag)
            "82",
            "1A98", //length --> this index shal be modified
            "A1",   //Biometric Header Template, where dd is the total length of the BHT
            "0D",   // this is the dd^^
            "81",   //Biometric type tag, (optional)
            "03",   //length of ^^
            "000002",//value of ^^ not sure about the length, this is more than 3, right?
            "87",   //format owner tag, required
            "02",   //length ^^
            "000A", //value ^^, again that length, are the leading 0's being ignored. solved below **
            "88",   //format type tag
            "02",   //length
            "0004", //value
            "5F2E", //biometric data tag
            "82",
            "1A84",  //length goes here. It turns out, its the length in bytes, not string length :)
            //--> this index shal be modified
            //face image here
            "dummy bio data, you know, to get space in array :)"
    };
    private static final int MAX_SIZE_KB = 9;

    private BufferedImage originalImage;
    //the initial file size of the image
    private long imageSize;

    private byte[] imageBytes;

    private String j2kHex;

    public DG2Provider(long imageSize, BufferedImage originalImage) {
        this.imageSize = imageSize;
        this.originalImage = originalImage;
    }

    public DG2Provider() {
    }

    /**
     * Get DG3 from a preexsiting hex representation of an image. Can be used witht the empty constructor
     * @param hex
     * @return
     */
    public String getDG2LDS(String hex) {
        //get imagebytes
        //convert to hex
        //add hex to index 26
        //get size of index 26, set to index 25
        DATA_PAYLOAD[26] = hex;

        int length = 0;

        //work from behind to calculate tag lengths
        for (int i = 26; i >= 0; i--) {

            if (i == 11 || i == 5 || i == 2 || i == 25) {
                DATA_PAYLOAD[i] = toHexString(length);
                System.out.println(length);
            }
            length += hexStringToByteArray(DATA_PAYLOAD[i]).length;


        }
        //construct the final string
        String res = "";
        for (int i = 0; i < DATA_PAYLOAD.length; i++) {
            res += DATA_PAYLOAD[i];
        }
        System.out.println(res.toUpperCase());

        return res.toUpperCase();
    }

    /**
     * Returns the DG2 LDS based on the input file supplied. Currently image wont be resized to below 9k, later. Should not be used with the empty constructor
     *
     * @return
     * @throws IOException
     */
    public String getDG2LDS() throws IOException {
        imageBytes = getImageBytes();//get the shrunkem image first, j2k
        String hex = toHexString(imageBytes);
        return getDG2LDS(hex);

    }

    private String toHexString(int data) {
        return Integer.toHexString(data);
    }

    /**
     * *Maintains a loop to progressively compress the image until the size is below the maximum allowed
     *
     * @return the compressed image bytes, J2K
     * @throws IOException
     */
    public byte[] getImageBytes() throws IOException {
        float quality = 1.0f;
        long size = imageSize;
        System.out.println("raw: " + size);

        if (size > MAX_SIZE_KB * 1024) {
            //required initial compression using the highest quality factor
            getImageBytes(quality);

            while (imageBytes.length > MAX_SIZE_KB * 1024) {
                //reduce quality factor progressively
                quality -= 0.1f;
                getImageBytes(quality);
                System.out.println("quality : " + quality);
            }
        }

        System.out.println("compressed: " + imageBytes.length);
        return imageBytes;

    }

    /**
     * Write to a byte outsput stream instead of file on local disk. use 1.0f for quality to avoid compression
     *
     * @param quality
     * @return
     * @throws IOException
     */
    public byte[] getImageBytes(float quality) throws IOException {
        //open the image then compress it, once more. the quality factor will be generated as before
        //first open image, have it as bufferedimage -- you should not directly open the file into a byte[]
        //BufferedImage originalImage = ImageIO.read(input);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Iterator writers = ImageIO.getImageWritersByFormatName("JPEG 2000");
        ImageWriter writer = (ImageWriter) writers.next();

        ImageOutputStream ios = ImageIO.createImageOutputStream(baos);
        writer.setOutput(ios);

        J2KImageWriteParam writeParams = (J2KImageWriteParam) writer.getDefaultWriteParam();
        writeParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        writeParams.setLossless(false);//favour reduction in file size
        writeParams.setCompressionType("JPEG2000");
        writeParams.setCompressionQuality(quality);
        writeParams.setEncodingRate(quality);

        writer.write(null, new IIOImage(originalImage, null, null), writeParams);
        baos.flush();

        System.out.println("ios len: " + ios.length());
        imageBytes = getBytes(ios);
        System.out.println("d len: " + imageBytes.length);

        ios.close();
        baos.close();
        writer.dispose();
        return imageBytes;
    }

    public byte[] getBytes(ImageOutputStream ios) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(255);
        long counter = 0;
        try {
            ios.seek(0);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        while (true) {
            try {
                bos.write(ios.readByte());
                counter++;
            } catch (EOFException e) {
                break;
            } catch (IOException e) {
                break;
            }
        }
        byte[] retValue = bos.toByteArray();
        return retValue;
    }

    /**
     * Name and signatrure SE
     *
     * @param bytes
     * @return
     */
    public String toHexString(byte[] bytes) {
        StringBuilder idStr = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {

            if ((bytes[i] & 0xFF) <= 15)
                idStr.append("0");

            idStr.append(
                    Integer.toHexString(bytes[i] & 0xFF).toUpperCase());
        }

        return idStr.toString();
    }

    public byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
