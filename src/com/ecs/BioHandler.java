package com.ecs;

import com.ecs.datagroups.DG2Provider;
import com.ecs.db.ContentValues;
import com.ecs.db.DbHelper;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Frederick on 1/20/2016.
 */
public class BioHandler extends DbHelper {
    public static final String TABLE_BIO = "bio";
    public static final String C_REF_ID = "ref_id";
    public static final String C_CS_FINGERPRINTS = "finger_prints";
    public static final String C_J2K_IMAGE = "j2k_image";
    public static final String C_ORIG_IMAGE = "orig_image";


    private String refId;
    private List<String> fingerPrints;
    private BufferedImage bufferedImage;
    private DG2Provider dg2Provider;
    private long imageSize;


    /**
     * @param refId
     * @param fingerPrints
     * @param bufferedImage//TODO convert your original bufferedimage to a byte[]
     * @param imageSize           the size of the original image in bytes. Required for calculation of required size
     */
    private void init(String refId, List<String> fingerPrints, BufferedImage bufferedImage, long imageSize) {
        this.refId = refId;
        this.fingerPrints = fingerPrints;
        this.bufferedImage = bufferedImage;
        this.imageSize = imageSize;

        dg2Provider = new DG2Provider(imageSize, bufferedImage);
    }

    /**
     * Inserts raw data into the db. Both original and j2k images will be converted to hex before being stored.
     *
     * @param refId
     * @param fingerPrints
     * @param bufferedImage
     * @param imageSize
     * @throws IOException
     * @throws SQLException
     */
    public void insertData(String refId, List<String> fingerPrints, BufferedImage bufferedImage, long imageSize) throws IOException, SQLException {
        init(refId, fingerPrints, bufferedImage, imageSize);

        //build comma separated fingerprint data
        String fingerPrintData = "";

        int index = 0;
        for (String fingerPrint : fingerPrints) {
            fingerPrintData += fingerPrint + (++index < fingerPrints.size() ? "," : "");
        }

        //get image info

        ContentValues values = new ContentValues();
        values.put(C_REF_ID, refId);
        values.put(C_CS_FINGERPRINTS, fingerPrintData);
        values.put(C_J2K_IMAGE, dg2Provider.toHexString(dg2Provider.getImageBytes()));
        values.put(C_ORIG_IMAGE, dg2Provider.toHexString(dg2Provider.getImageBytes(1.0f)));//1.0f implies best quality, TODO convert orig to hex

        insert(TABLE_BIO, values);


    }

    /**
     * Gets data from the table. should be pre-existing, checks are omitted.
     *
     * @return DG@ hex concated to DG3 and transformed to a byte array
     */
    public byte[] getEncodedData(String refId) throws SQLException {
        //DG2 work
        ResultSet rs = query(new String[]{TABLE_BIO}, "where " + C_REF_ID + "=?", new String[]{refId});
        DG2Provider provider = new DG2Provider();
        String dg2 = provider.getDG2LDS(rs.getString(C_J2K_IMAGE));

        //DG3 business
        String fingerPrints = rs.getString(C_CS_FINGERPRINTS);
        String[] prints = fingerPrints.split(",");

        List<String> fingerPrintsList = new ArrayList<String>();
        for (int i = 0; i < prints.length; i++) {
            fingerPrintsList.add(prints[i]);
        }
        rs.close();
        Utils utils = new Utils(fingerPrintsList);
        String dg3 = utils.createDg3(); //???

        return provider.hexStringToByteArray(dg2 + dg3);
    }


}
