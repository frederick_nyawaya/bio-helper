package com.ecs;

import com.ecs.datagroups.DG2Provider;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Created by Frederick on 1/22/2016.
 */
public class Go {


    public static void main(String... args) throws IOException {


//        ImageHandler handler = new ImageHandler(ImageHandler.SOURCE_WEBCAM, new File("D:\\out"), null, new File("D:\\compressed"));
//        handler.execute();

        DG2Provider dg2Provider = new DG2Provider(10000000L, ImageIO.read(new File("D:\\compressed")));
        String hex = dg2Provider.toHexString(dg2Provider.getImageBytes());
        System.out.println(hex);
        System.out.println(dg2Provider.getDG2LDS(hex));


    }
}
