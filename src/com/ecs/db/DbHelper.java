package com.ecs.db;

import java.sql.*;
import java.util.List;

/**
 * Created by Frederick on 1/20/2016.
 */
public class DbHelper {
    /**
     *
     * @param tables
     * @param where
     * @param whereArgs
     * @return
     * @throws SQLException
     */
    public ResultSet query(String[] tables, String where, String whereArgs[]) throws SQLException {
        Connection conn = DriverManager.getConnection(Config.URL, Config.USER_NAME, Config.PASSWORD);

        String tablesString = "";

        for (int i = 0; i < tables.length; i++) {
            tablesString += (tables[i] + (i < (tables.length - 1) ? "," : ""));
        }

        PreparedStatement stmt = conn.prepareStatement("select * from " + tablesString + " " + where);

        for (int i = 1; i <= whereArgs.length; i++) {
            stmt.setString(i, whereArgs[i - 1]);
        }

        System.out.println(stmt.toString());

        return stmt.executeQuery();
    }
    public int insert(String table, ContentValues values) throws SQLException {
        List<ContentValues.KeyValue> valuesList = values.getValues();
        String columns = "(";
        String valuesString = "(";
        for (ContentValues.KeyValue keyValue : valuesList) {
            columns += keyValue.key;
            System.out.println("KEY -- " + keyValue.key + " VALUE -- " + keyValue.value);

            if (keyValue.value.equals("sysdate")) {
                valuesString += "sysdate ";
            } else {
                valuesString += "?";

            }
            if (valuesList.indexOf(keyValue) < valuesList.size() - 1) {
                columns += ",";
                valuesString += ",";
            } else {
                columns += ")";
                valuesString += ")";

            }

        }

        return insert(table, columns, valuesString, values);

    }

    private int insert(String table, String columns, String values, ContentValues cvs) throws SQLException {
        String statement = "INSERT INTO " + table + columns + " VALUES" + values;
        Connection conn = DriverManager.getConnection(Config.URL, Config.USER_NAME, Config.PASSWORD);
        PreparedStatement stmt = conn.prepareStatement(statement);
        List<ContentValues.KeyValue> valuesList = cvs.getValues();
        int i = 1;
        for (ContentValues.KeyValue kv : valuesList) {
            if (!kv.value.equals("sysdate")) {
                stmt.setString(i++, kv.value);
            }

        }

        int success = stmt.executeUpdate();

        //all good, though should be done in a try-catch-finally
        stmt.close();
        conn.close();

        System.out.println("inserted sumtin");
        return success;
    }


}
