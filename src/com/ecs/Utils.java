package com.ecs;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Utils {
	private static final String A1_TAG="A1118101088201018702010188020201F90100";
	private static  String _tag_template_len;
	private static List <String>fingerPrinttemplate;
	public static void main(String[] args) {
		generateDg3();
	
	}
	public Utils(List<String> fingerPrinttemplate)
	{
		Utils.fingerPrinttemplate=fingerPrinttemplate;
		Utils._tag_template_len="02010"+fingerPrinttemplate.size();
	}
public static void generateDg3()
{
	try {
	    String template;
		Scanner scanner = new Scanner(new File("C:\\bio\\biopath\\samplebio.BIO"));
		while (scanner.hasNext()) 
		{
		     template=scanner.nextLine();
		     fingerPrinttemplate.add(template);
		     
		}
		scanner.close();
		
	
	} catch (Exception e) {
		System.out.println(e);
		// TODO: handle exception
	}
}
  public String createDg3()
  {
		String concat,fingers;
		StringBuilder builder=new StringBuilder();
		for(String l:fingerPrinttemplate)
		{
			fingers=getFingerPrintLen(l);
		    concat=fingers+A1_TAG;
			builder.append(get7F60tag(concat)+fingers);
		}
		return get63Tag(get7F61tag(builder.toString()));
	  
  }
	private static String getFingerPrintLen(String str)
	{
		String tag="";
		
	     if ((((double) getStringLen(str)) / 2.0) <=255.0){
			tag="5F2E81"+ConvertHEX(getByteslen(str.getBytes()))+str;
		}
		else{
			tag = "5F2E82"+ConvertHEX(getByteslen(str.getBytes()))+str;
		}
		return tag;
	}
	private static String get7F60tag(String s)
	{ 
	 String tag="";
	 int len=getByteslen(s.getBytes());
	 int num2;
	
	  if ((((double) len) / 2.0) <= 255.0)
	 {
		 num2=2;
	 }
	 else{
		 num2=3;
	 }
	 num2 = (int) Math.round((double) ((num2 + 0x15) + (((double)len) / 2.0)));
	
      if (num2 <= 0xff)
     {
         tag = "7F6081"+ConvertHEX(getByteslen(s.getBytes()))+A1_TAG;
     }
     else
     {
         tag = "7F6082"+ConvertHEX(getByteslen(s.getBytes()))+A1_TAG;
     }
	 return tag;
	}
	public static int getByteslen(byte[] bytes) {
		return (bytes.length) / 2;
	}
    public static String get7F61tag(String str)
    {
    	String expression="";
    	
    	
    	 int len=getByteslen((str+_tag_template_len).getBytes());
    	
    	 int  num2 = (int) Math.round((double) (3.0 + (((double)len) / 2.0)));
         
        if (num2 <= 0xff)
          {
              expression = "7F6181" + ConvertHEX(len) + _tag_template_len + str;
          }
          else
          {
              expression = "7F6182" + ConvertHEX(len) + _tag_template_len + str;
          }
        return expression;
    }
	private static String ConvertHEX(int Val) {
		String str = "";
		String expression = Integer.toHexString(Val);
		switch (getStringLen(expression))
		{
		case 0:
			str = "00";
			break;
		case 1:
		case 3:
			str = "0" + expression;
			break;
		case 2:
		case 4:
			str = expression;
			break;
		}

		return str.toUpperCase();
	}

	private static int getStringLen(String str) {
		return str.length();
	}
	private static String get63Tag(String str)
	{
		 String _tag="";
		 int len=getByteslen(str.getBytes());
		 int  num2 = (int) Math.round((double) (((double)len) / 2.0));
          
            if (num2 <= 0xff)
           {
            	_tag = "6381" + ConvertHEX(len) + str;
           }
           else
           {
        	   _tag = "6382" +ConvertHEX(len) + str;
           }
            return _tag;
	}
 
    public String createEF06(String adress1, String address2, String address3, String holdername, String nid, String socialcode, String hudumaid, String RFU) {
        StringBuilder b = new StringBuilder();
        int requiredSize = 100;
        b.append(padString(adress1, requiredSize));
        requiredSize = 100;
        b.append(padString(address2, requiredSize));
        requiredSize = 100;
        b.append(padString(address3, requiredSize));
        requiredSize = 30;
        b.append(padString(holdername, requiredSize));
        requiredSize = 20;
        b.append(padString(nid, requiredSize));
        requiredSize = 20;
        b.append(padString(socialcode, requiredSize));
        requiredSize = 20;
        b.append(padString(hudumaid, requiredSize));
        requiredSize = 110;
        b.append(padString(RFU, requiredSize));
        return b.toString();
    }
    String paddedString;
    public String stringToHex(String input) throws UnsupportedEncodingException {
        if (input == null) {
            throw new NullPointerException();
        }
        return asHex(input.getBytes()).toUpperCase();
    }

    private String asHex(byte[] buf) {
        char[] chars = new char[2 * buf.length];
        for (int i = 0; i < buf.length; ++i) {
            chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
            chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
        }
        return new String(chars);
    }
    private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();
    public String padString(String string, int requiredSize) {
        try {
            
            paddedString = StringUtils.rightPad(string.trim(), requiredSize, " ");

            return stringToHex(paddedString);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
