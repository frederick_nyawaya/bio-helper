
package com.ecs.handler.data;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VerificationByPassportResult" type="{http://schemas.datacontract.org/2004/07/IPRSManager}FingerprintVerificationResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "verificationByPassportResult"
})
@XmlRootElement(name = "VerificationByPassportResponse")
public class VerificationByPassportResponse {

    @XmlElementRef(name = "VerificationByPassportResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<FingerprintVerificationResult> verificationByPassportResult;

    /**
     * Gets the value of the verificationByPassportResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FingerprintVerificationResult }{@code >}
     *     
     */
    public JAXBElement<FingerprintVerificationResult> getVerificationByPassportResult() {
        return verificationByPassportResult;
    }

    /**
     * Sets the value of the verificationByPassportResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FingerprintVerificationResult }{@code >}
     *     
     */
    public void setVerificationByPassportResult(JAXBElement<FingerprintVerificationResult> value) {
        this.verificationByPassportResult = value;
    }

}
