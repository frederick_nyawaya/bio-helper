
package com.ecs.handler.data;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PingServiceResult" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pingServiceResult"
})
@XmlRootElement(name = "PingServiceResponse")
public class PingServiceResponse {

    @XmlElement(name = "PingServiceResult")
    protected Boolean pingServiceResult;

    /**
     * Gets the value of the pingServiceResult property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPingServiceResult() {
        return pingServiceResult;
    }

    /**
     * Sets the value of the pingServiceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPingServiceResult(Boolean value) {
        this.pingServiceResult = value;
    }

}
