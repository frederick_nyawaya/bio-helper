
package com.ecs.handler.data;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for FingerprintVerificationResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FingerprintVerificationResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/IPRSManager}CustomError">
 *       &lt;sequence>
 *         &lt;element name="Result" type="{http://schemas.datacontract.org/2004/07/IPRSManager}FingerprintVerificationResultsSet" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FingerprintVerificationResult", namespace = "http://schemas.datacontract.org/2004/07/IPRSManager", propOrder = {
    "result"
})
public class FingerprintVerificationResult
    extends CustomError
{

    @XmlElement(name = "Result")
    @XmlSchemaType(name = "string")
    protected FingerprintVerificationResultsSet result;

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link FingerprintVerificationResultsSet }
     *     
     */
    public FingerprintVerificationResultsSet getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link FingerprintVerificationResultsSet }
     *     
     */
    public void setResult(FingerprintVerificationResultsSet value) {
        this.result = value;
    }

}
