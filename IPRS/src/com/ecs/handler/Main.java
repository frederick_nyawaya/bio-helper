package com.ecs.handler;

import com.ecs.handler.data.GetDataByIdCardResponse;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;

/**
 * Created by Frederick on 1/29/2016.
 */
public class Main {
    private static final String URL = "http://automated-revenue-system.appspot.com/testMail";

    private static final String USER_AGENT = "me";

    public static void main(String... argv) {
        try {
            sendPost("data");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static GetDataByIdCardResponse getDataById(String idNumber, String serialNumber) throws Exception {
        return new Gson().fromJson(
                sendPost(Constants.REQUEST + "=" + Constants.ACTION_DATA_BY_ID + "&" + Constants.SERIAL_NUMBER + "=" + serialNumber + "&" + Constants.ID_NUMBER + "=" + idNumber), GetDataByIdCardResponse.class);
    }

    public static synchronized String sendPost(String data) throws Exception {
        URL obj = new URL(URL);
        String encoding = Base64.encodeBase64String(("admin:motdepasse").getBytes());

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Authorization", "Basic " + encoding);

        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        //for other type of access restriction
        con.setRequestProperty("Auth-Key", "auth key, unused for now");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(data);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(
                con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // print result
        System.out.println("responseCode - " + responseCode);

        System.out.println("response - " + response);
        if (responseCode == 200)
            return response.toString();
        else
            return null;

    }
}
