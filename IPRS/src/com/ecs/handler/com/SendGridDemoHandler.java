package com.ecs.handler.com;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class SendGridDemoHandler {

 public static void main(String... argv){
     try {
         System.out.println("sending..");

         send();
         System.out.println("sent");

     } catch (Exception e) {
         e.printStackTrace();
         System.out.println("failed " + e.getMessage());

     }


 }

  private static final String SMTP_HOST_NAME = "smtp.sendgrid.net";
  private static final String SMTP_AUTH_USER = "bungomacounty";
  private static final String SMTP_AUTH_PWD = "desmosedici2090";
  private static final int SMTP_PORT = 2525;

  private static void send() throws Exception {
    Properties props = new Properties();
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.auth", "true");

    Authenticator auth = new SMTPAuthenticator();
    Session mailSession = Session.getDefaultInstance(props, auth);
    Transport transport = mailSession.getTransport();

    MimeMessage message = new MimeMessage(mailSession);

    Multipart multipart = new MimeMultipart("alternative");

    // Sets up the contents of the email message
    BodyPart part1 = new MimeBodyPart();
    part1.setText("message");

    multipart.addBodyPart(part1);

    message.setContent(multipart);
    message.setFrom(new InternetAddress("o.frederickn@gmail.com"));
    message.setSubject("subject");
    message.addRecipient(
        Message.RecipientType.TO, new InternetAddress("o.frederickn@gmaail.com"));

    // Sends the email
    transport.connect(SMTP_HOST_NAME, SMTP_PORT, SMTP_AUTH_USER, SMTP_AUTH_PWD);
    transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
    transport.close();
  }

  // Authenticates to SendGrid
  private static class SMTPAuthenticator extends javax.mail.Authenticator {
    @Override
    public PasswordAuthentication getPasswordAuthentication() {
      String username = SMTP_AUTH_USER;
      String password = SMTP_AUTH_PWD;
      return new PasswordAuthentication(username, password);
    }
  }
}